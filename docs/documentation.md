---
sidebar_position: 2
---

# Building documentation

The documentation job triggers a new build in the [abfelbaum/website](https://git.abfelbaum.dev/abfelbaum/website) project when a file in the `docs/` folder has changed.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "documentation.yml"
```