# Default

These are the default rules for common functionality regarding GitLab CI.

| Rule name                                       | What it does                                                                      |
|-------------------------------------------------|-----------------------------------------------------------------------------------|
| `.default`                                      | Default rule for CI jobs. Executes only on specific triggers to prevent conflicts |
| `.default_branch`                               | All of the `.default` rule but executes only on the default branch                |
| `.requires_tag`                                 | Fails if `$CI_COMMIT_TAG` is not set                                              |
| `.is_tag`                                       | Executes if `$CI_COMMIT_TAG` is set                                               |
| `.not_tag`                                      | Fails if `$CI_COMMIT_TAG` is set                                                  |
| `.requires_default_branch`                      | Fails if `$CI_COMMIT_BRANCH` is not equal to `$CI_DEFAULT_BRANCH`                 |
| `.is_default_branch`                            | Executes if `$CI_COMMIT_BRANCH` is equal to `$CI_DEFAULT_BRANCH`                  |
| `.not_default_branch`                           | Fails if `$CI_COMMIT_BRANCH` is equal to `$CI_DEFAULT_BRANCH`                     |
| `.requires_trigger_api`                         | Fails if `$CI_PIPELINE_SOURCE` is not set to `api`                                |
| `.is_trigger_api`                               | Executes if `$CI_PIPELINE_SOURCE` is set to `api`                                 |
| `.not_trigger_api`                              | Fails if `$CI_PIPELINE_SOURCE` is set to `api`                                    |
| `.requires_trigger_chat`                        | Fails if `$CI_PIPELINE_SOURCE` is not set to `chat`                               |
| `.is_trigger_chat`                              | Executes if `$CI_PIPELINE_SOURCE` is set to `chat`                                |
| `.not_trigger_chat`                             | Fails if `$CI_PIPELINE_SOURCE` is set to `chat`                                   |
| `.requires_trigger_external`                    | Fails if `$CI_PIPELINE_SOURCE` is not set to `external`                           |
| `.is_trigger_external`                          | Executes if `$CI_PIPELINE_SOURCE` is set to `external`                            |
| `.not_trigger_external`                         | Fails if `$CI_PIPELINE_SOURCE` is set to `external`                               |
| `.requires_trigger_external_pull_request_event` | Fails if `$CI_PIPELINE_SOURCE` is not set to `external_pull_request_event`        |
| `.is_trigger_external_pull_request_event`       | Executes if `$CI_PIPELINE_SOURCE` is set to `external_pull_request_event`         |
| `.not_trigger_external_pull_request_event`      | Fails if `$CI_PIPELINE_SOURCE` is set to `external_pull_request_event`            |
| `.requires_trigger_merge_request_event`         | Fails if `$CI_PIPELINE_SOURCE` is not set to `merge_request_event`                |
| `.is_trigger_merge_request_event`               | Executes if `$CI_PIPELINE_SOURCE` is set to `merge_request_event`                 |
| `.not_trigger_merge_request_event`              | Fails if `$CI_PIPELINE_SOURCE` is set to `merge_request_event`                    |
| `.requires_trigger_parent_pipeline`             | Fails if `$CI_PIPELINE_SOURCE` is not set to `parent_pipeline`                    |
| `.is_trigger_parent_pipeline`                   | Executes if `$CI_PIPELINE_SOURCE` is set to `parent_pipeline`                     |
| `.not_trigger_parent_pipeline`                  | Fails if `$CI_PIPELINE_SOURCE` is set to `parent_pipeline`                        |
| `.requires_trigger_pipeline`                    | Fails if `$CI_PIPELINE_SOURCE` is not set to `pipeline`                           |
| `.is_trigger_pipeline`                          | Executes if `$CI_PIPELINE_SOURCE` is set to `pipeline`                            |
| `.not_trigger_pipeline`                         | Fails if `$CI_PIPELINE_SOURCE` is set to `pipeline`                               |
| `.requires_trigger_push`                        | Fails if `$CI_PIPELINE_SOURCE` is not set to `push`                               |
| `.is_trigger_push`                              | Executes if `$CI_PIPELINE_SOURCE` is set to `push`                                |
| `.not_trigger_push`                             | Fails if `$CI_PIPELINE_SOURCE` is set to `push`                                   |
| `.requires_trigger_schedule`                    | Fails if `$CI_PIPELINE_SOURCE` is not set to `schedule`                           |
| `.is_trigger_schedule`                          | Executes if `$CI_PIPELINE_SOURCE` is set to `schedule`                            |
| `.not_trigger_schedule`                         | Fails if `$CI_PIPELINE_SOURCE` is set to `schedule`                               |
| `.requires_trigger_trigger`                     | Fails if `$CI_PIPELINE_SOURCE` is not set to `trigger`                            |
| `.is_trigger_trigger`                           | Executes if `$CI_PIPELINE_SOURCE` is set to `trigger`                             |
| `.not_trigger_trigger`                          | Fails if `$CI_PIPELINE_SOURCE` is set to `trigger`                                |
| `.requires_trigger_web`                         | Fails if `$CI_PIPELINE_SOURCE` is not set to `web`                                |
| `.is_trigger_web`                               | Executes if `$CI_PIPELINE_SOURCE` is set to `web`                                 |
| `.not_trigger_web`                              | Fails if `$CI_PIPELINE_SOURCE` is set to `web`                                    |
| `.requires_trigger_webide`                      | Fails if `$CI_PIPELINE_SOURCE` is not set to `webide`                             |
| `.is_trigger_webide`                            | Executes if `$CI_PIPELINE_SOURCE` is set to `webide`                              |
| `.not_trigger_webide:`                          | Fails if `$CI_PIPELINE_SOURCE` is set to `webide:`                                |