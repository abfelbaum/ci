# Release

These rules check if a tag corresponds to a valid semantic versioned release.

## `.is_release_stable`

The rule checks if a tag name matches the semantic versioning pattern of stable releases (i.e. without `-beta`,
only `1.0.0`).

It only is triggered by newly pushed tags.

## `.is_release_preview`

The rule checks if a tag name matches the semantic versioning pattern of prereleases (i.e. with `-beta`, `1.0.0-beta`).

It only is triggered by newly pushed tags.

## `.release`

This is a shorthand for the following rules:

* [`.is_release_stable`](#is_release_stable)
* [`.is_release_preview`](#is_release_preview)
* [`.is_scheduled_container_update_legacy`](scheduled/container-update#is_scheduled_container_update_legacy)
* [`.is_scheduled_container_update_latest`](scheduled/container-update#is_scheduled_container_update_latest)

## `.release_latest`

This is a shorthand for the following rules:

* [`.is_release_stable`](#is_release_stable)
* [`.is_scheduled_container_update_latest`](scheduled/container-update#is_scheduled_container_update_latest)

## `.release_legacy`

This is a shorthand for the following rules:

* [`.is_release_stable`](#is_release_stable)
* [`.is_scheduled_container_update_legacy`](scheduled/container-update#is_scheduled_container_update_legacy)