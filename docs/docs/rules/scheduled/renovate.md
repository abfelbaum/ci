# Renovate

These rules check if a pipeline is a pipeline for renovate.

To use container update pipelines please create a schedule with the following environment variable:

```dotenv
SCHEDULE_RENOVATE=true
```

## `.is_scheduled_renovate`

This rule checks if the `SCHEDULE_RENOVATE` variable is set.