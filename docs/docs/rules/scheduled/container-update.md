# Container updates

These rules check if a pipeline is a pipeline for container updates.

To use container update pipelines please create a schedule with the following environment variable:

```dotenv
SCHEDULE_CONTAINER_UPDATE=true
```

## `.is_scheduled_container_update`

This rule checks if the `SCHEDULE_CONTAINER_UPDATE` variable is set.

## `.is_scheduled_container_update_legacy`

This rule checks if the pipeline is a container update and if `SCHEDULE_CONTAINER_UPDATE_LATEST` is `false`.

## `.is_scheduled_container_update_latest`

This rule checks if the pipeline is a container update and if `SCHEDULE_CONTAINER_UPDATE_LATEST` is `true`.