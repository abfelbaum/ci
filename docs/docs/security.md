# Security scanning

`security.yml` includes the most important GitLab security (and compliance) scanners.

## Getting started

To include the security scans in your project include the `security.yml` in your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "security.yml"
```

## Configuration

Since these scanners are built in the configuration is documented at GitLab.

| Job                   | Documentation                                                             |
|-----------------------|---------------------------------------------------------------------------|
| `dependency_scanning` | https://docs.gitlab.com/ee/user/application_security/dependency_scanning/ |
| `*-sast`              | https://docs.gitlab.com/ee/user/application_security/sast/                |
| `secret_detection`    | https://docs.gitlab.com/ee/user/application_security/secret_detection/    |
| `license_scanning`    | https://docs.gitlab.com/ee/user/compliance/license_compliance/            |

Additionally another configuration option is provided.

| Variable                 | Default value | Description                                                                                                          |
|--------------------------|---------------|----------------------------------------------------------------------------------------------------------------------|
| `DISABLE_SECURITY_SCANS` | `false`       | Disables **ALL** included scans when `true`. Useful for scheduled pipelines like renovate which does not need scans. |

## Disable job

To disable a specific job you can add a rule that never equals true to it

```yaml title=".gitlab-ci.yml"
dependency_scanning:
  rules:
    - when: never
```