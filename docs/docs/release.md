# Releasing versions

The `release.yml` makes use of `semantic-release` and GitLabs `release-cli` to automatically generate changelogs, tags
and releases.

## Getting started

### Configuration

#### Branches

The `semantic-release` tool requires a `main` branch to be present. You do not have to use this branch and can prevent
anyone from creating commits there, it just needs to be present for the tool to run.

#### Configuration file

Configuration happens via the `release.config.js` file. If none is provided, a default will be injected into the ci:

```js title="release.config.js"
const branch = process.env.CI_COMMIT_BRANCH
const isStableRelease = process.env.STABLE_RELEASE === "true";

const config = {
    tagFormat: "${version}",
    branches: [
        "+([0-9])?(.{+([0-9]),x}).x",
        "main",
        {
            name: "dev",
            channel: "default",
            prerelease: isStableRelease ? false : "beta"
        }
    ],
    plugins: [
        ["@semantic-release/commit-analyzer", {
            preset: "conventionalcommits"
        }]
    ]
}

if (config.branches.some(it => it === branch || (it.name === branch && !it.prerelease))) {

    config.plugins.push(["@semantic-release/release-notes-generator", {
        preset: "conventionalcommits"
    }]);

    config.plugins.push([ "@semantic-release/gitlab", {
        labels: "Type::Bugfix,Priority::High"
    }]);

    config.plugins.push([ "@semantic-release/changelog", {
        changelogTitle: "# Changelog\n\nAll notable changes to this project will be documented in this file. See\n[Conventional Commits](https://conventionalcommits.org) for commit guidelines."
    }]);

    config.plugins.push([ "@semantic-release/git", {
        message: "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes} [skip ci]"
    }]);
}

if(branch !== "main" && branch !== "dev") {
    config.branches.push({
        name: branch,
        channel: "default",
        prerelease: "alpha"
    })
}

module.exports = config
```

This will configure semantic release to:

* Use [ConventionalCommits](https://www.conventionalcommits.org/en/v1.0.0/)
* Create `beta` releases from `dev` branch and `stable` releases when the `STABLE_RELEASE` environment variable is set
  to `true` (done via manual pipeline)
* Create `alpha` releases from any other branch
* Create specific version releases from `major.x` or `major.minor.x` branches
* Generate changelog and GitLab releases only on stable releases

### GitLab CI

When you configured `semantic-release` you can add the `release.yml` to your GitLab CI

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "release.yml"
```

It is using the `deploy` stage.

:::info

If you prefer to not use the GitLab plugin, feel free to comment it out or remove it from the configuration file.

:::

Now, for every push you have a pipeline named `semantic-release:deploy:prerelease`. The following happens there:

1. `semantic-release` analyzes the commit messages and determines the next version
3. `semantic-release` creates a new tag

On `dev` branch you also have a manual stage named `semantic-release:deploy:prerelease`. The following happens on
trigger

1. `semantic-release` analyzes the commit messages and determines the next version
2. `semantic-release` generates `CHANGELOG.md`
3. `semantic-release` creates a new tag
4. `semantic-release` creates a new release with the tag name as title and the commit message as description
5. `semantic-release` comments to every issue that was resolved in that release that it has been resolved
