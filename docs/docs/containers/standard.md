---
description: "Building and releasing of containers with waypoint."
---

# Standard container CI

This ci file handles building and releasing of containers with waypoint.

:::info

To re-tag containers please refer to [my retag CI](retag.md).

:::

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "containers/standard.yml"
```

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                                  | Type     | 
|-------------------------------------------------------|----------|
| [Release](../release.md)                              | CI       | 
| [Rules](../rules)                                     | CI       | 
| [Containers Security](security.md)                    | CI       | 
| [Containers update](update.md)                        | CI       | 
| [Containers re-tag](../templates/containers/retag.md) | Template |
| [waypoint build](../templates/waypoint/build.md)      | Template | 
| [waypoint deploy](../templates/waypoint/deploy.md)    | Template |

Additional configuration

| Variable   | Default value                           | Description                                                                                                                                        |
|------------|-----------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| `CS_IMAGE` | `${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}` | The container image that should be scanned by the `container_scanning` job. Has to always refer to the image with the current commit hash as a tag |

## What it does

The template triggers at every commit a container build.

For every pipeline a manual job for releasing is added.

On a release a docker container is built and pushed to the container registry. For stable releases the container is then
deployed to production.