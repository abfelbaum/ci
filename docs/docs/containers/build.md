---
description: "Building and publishing of containers with waypoint."
---

# Build

This ci file handles building and publishing of containers with waypoint.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "containers/build.yml"
```

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                               | Type     | 
|----------------------------------------------------|----------|
| [Containers Security](security.md)                 | CI       | 
| [Containers re-tag](retag.md)                      | CI       |
| [waypoint build](../templates/waypoint/build.md)   | Template | 
| [waypoint deploy](../templates/waypoint/deploy.md) | Template |

Additional configuration

| Variable   | Default value                           | Description                                                                                                                                        |
|------------|-----------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| `CS_IMAGE` | `${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}` | The container image that should be scanned by the `container_scanning` job. Has to always refer to the image with the current commit hash as a tag |

## What it does

The template triggers at every commit a container build and a security scan.

On `dev` branch the container will be pushed to the container registry as the `latest` tag.
