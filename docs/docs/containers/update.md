---
description: "Update containers frequently."
---

# Update

Containers contain an os that requires frequent security updates. Because containers cannot be updated permanently
easily each container should be rebuilt every 24h.

## Getting started

### GitLab CI

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "containers/update.yml"
```

### Scheduled Pipeline

Please follow
the [official documentation](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#add-a-pipeline-schedule) to create a
pipeline schedule.

My recommendation for an interval is *every day* but *every week* should be okay too when you don"t want to waste
resources.

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                                                 | Type     | 
|----------------------------------------------------------------------|----------|
| [Rules](../rules)                                                    | Template | 
| [containers update latest](../templates/containers/update/latest.md) | Template | 
| [containers update legacy](../templates/containers/update/legacy.md) | Template |

## What it does

When a scheduled pipeline is triggered a script creates a pipeline for every stable versioned git tag in the project.

[Rules](../rules) can be used to easily configure tag jobs to be- or not be run on scheduled pipelines.
