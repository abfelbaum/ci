# Renovate

The `renovate.yml` makes use of the [renovate bot](https://github.com/renovatebot/renovate) to notify about package
updates.

## Getting started

### GitLab CI

Add the `renovate.yml` to your GitLab CI

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "renovate.yml"
```

It is using the `test` stage.

### Schedule

Renovate runs per default on a scheduled pipeline with the `SCHEDULE_RENOVATE` environment variable set to `true`.

Please have a look at the [official GitLab documentation](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) on how
to create a scheduled pipeline. It is recommended to run renovate daily.

If a [security](security.md) pipeline is included you can disable it for the schedule by [configuring](security.md#configuration) it accordingly.

### Setup

When you have set up the [ci file](#gitlab-ci) and [schedule](#schedule) run
your [schedule manually](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#run-manually) once.

Renovate will create a merge request containing its configuration. Modify it according to your needs and merge.