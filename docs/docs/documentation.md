# Documentation

The documentation job triggers when a file in the `docs/` folder has changed.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "documentation.yml"
```

### Configure your project

Your project has to contain a `docs` folder for the docusaurus configuration. This folder will be copied into the
docusaurus base directory, so any file can overwrite original docusaurus files such as `docusaurus.config.js`.

In normal conditions there is no need to overwrite `docusaurus.config.js` because the most used configuration options
are exposed to files.

#### Navbar

To configure the navigation bar create a `themeconfig.js` in the `docs` folder.

```ts title="themeconfig.js"
/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/preset-classic').ThemeConfig} */
const themeConfig = {
  navbar: {
    title: process.env.DOCUSAURUS_TITLE,
    logo: {
      alt: 'Docusaurus Logo',
      src: 'img/docusaurus.png',
    },
    items: [
      {
        href: process.env.DOCUSAURUS_PROJECT_URL,
        label: 'GitLab',
        position: 'right',
      },
    ],
  }
};

module.exports = themeConfig;
```

Other theme options can be set here as well.

#### Sidebar

To configure the sidebar create a `sidebars.js` in the `docs` folder.

```ts title="sidebars.js"
/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  // By default, Docusaurus generates a sidebar from the docs folder structure
  default: [{type: 'autogenerated', dirName: '.'}],

  // But you can create a sidebar manually
  /*
  default: [
    'intro',
    'hello',
    {
      type: 'category',
      label: 'Tutorial',
      items: ['tutorial-basics/create-a-document'],
    },
  ],
   */
};

module.exports = sidebars;
```

#### Content

The content of the documentation will be located inside a `docs` folder that within the `docs` folder. You can also
create other docusaurus default paths like `static`. Please note that the blogs plugin is not enabled by default.

Your folder structure should look like this in the end:

```tree
.

├── docs
│   ├── docs
│   │   ├── index.md
│   ├── static
│   │   ├── img
│   │   │   ├── docusaurus.png
│   ├── themeconfig.js
│   └── sidebar.js
```

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                                | Type     | Notes |
|-----------------------------------------------------|----------|-------|
| [Rules](rules)                                      | CI       | -     |
| [Security](security.md)                             | CI       | -     |
| [Docusaurus](templates/documentation/docusaurus.md) | Template | -     |

## Features

We provide some extensions to the docusaurus features.

### RapiDoc

You can easily use a documentation page as an OpenApi documentation with RapiDoc:

```mdx title="api.mdx"
---
full_width: true
hide_title: true
---

import RapiDoc from "@site/src/components/RapiDoc.js";

<RapiDoc spec="https://api.app.rwth-aachen.de/v3/ron/swagger/v3/swagger.json"/>
```

It is recommended to use the `full_width` and `hide_title` front matter for a cleaner page look.

#### Known issues

##### Firefox displaying padding on the side

Since Firefox does not support `:has` css selectors it currently displays some more paddings on the sides of some
elements.