# Standard .NET CI

This ci file handles everything from building to deploying a .NET project with nomad.

:::info

To build NuGet packages please refer to [my NuGet CI](nuget.md).

:::

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "dotnet/standard.yml"

variables:
  DOTNET_PROJECT_PATH: "path/to/your/projectfile.csproj"
  DOTNET_TEST_PROJECT_PATH: "path/to/your/testprojectfile.csproj"
```

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                           | Type     | 
|------------------------------------------------|----------|
| [Release](../release.md)                       | CI       | 
| [Rules](../rules)                              | CI       | 
| [Security](../security.md)                     | CI       |
| [.NET build](../templates/dotnet/build.md)     | Template | 
| [.NET test](../templates/dotnet/test.md)       | Template |
| [Levant deploy](../templates/levant/deploy.md) | Template |

## What it does

The template triggers at every commit a .NET build. A release build can be triggered manually.

For every pipeline a manual job for releasing is added.

On a release the application is built and deployed to staging. For stable releases the application is then
deployed to production.