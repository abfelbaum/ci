# GitLab release-cli

This template makes use of the [GitLab `release-cli`](https://gitlab.com/gitlab-org/release-cli) to create
a [GitLab Release](https://docs.gitlab.com/ee/user/project/releases/).

The job uses the following environment variables for creating a new release:

* `CI_COMMIT_TAG` is used for the release title and the associated tag
* `CI_COMMIT_MESSAGE` is used for the release description

Since it is a release job the job is only executed on `tags` pipelines. To prevent it from being executed by schedules
or other automatic pipelines (i.e. [containers/update](../../containers/update.md)) it also is not executed
on `pipelines` and `scheduled`

## Customize

You can use this template for your own pipelines by extending the template job

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/release/gitlab.yml"

gitlab:release:deploy:
  extends: .gitlab:release:deploy
  
  release:
    name: Different release name
    description: With a cool description
```

