# semantic-release

This template makes use of [semantic-release](https://github.com/semantic-release/semantic-release).

## Getting started

### Configuration

The job expects configured variables for commiting, authenticating and signing commits.

| Variable                     | Purpose                                                 |
|------------------------------|---------------------------------------------------------|
| `SEMANTIC_RELEASE_ARGUMENTS` | Additional arguments for the `semantic-release` command |

#### Commiting

You *need* to configure the following environment variables for creating commits. I am not entirely sure which variables
are used by `semantic-release` but I really don't care since I"ve set up them once and it works.

| Variable              | Purpose                       |
|-----------------------|-------------------------------|
| `GIT_AUTHOR_EMAIL`    | The email of the git author   | 
| `GIT_AUTHOR_NAME`     | The name of the git author    |
| `GIT_COMMITTER_EMAIL` | The email of the git commiter |
| `GIT_COMMITTER_NAME`  | The name of the git commiter  |
| `GIT_EMAIL`           | The git email                 |
| `GIT_USERNAME`        | The git username              |

To help with setting these the environment variables `RELEASE_GIT_EMAIL` and `RELEASE_GIT_NAME` are mapped to the
corresponding variables listed above.

#### Authentication

##### git

Please refer to
the [official documentation](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/ci-configuration.md#authentication).

I am just using the `GIT_CREDENTIALS` environment variable since it is the easiest and fastest way.

##### GitLab

Please refer to the [official documentation](https://github.com/semantic-release/gitlab#gitlab-authentication).

## Customize

You can use this template for your own pipelines by extending the template job

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/release/semantic-release.yml"

semantic-release:deploy:
  extends: .semantic-release:deploy

  after_script:
    - echo "done!"
```