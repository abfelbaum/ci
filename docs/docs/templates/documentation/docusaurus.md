# Docusaurus

The docusaurus template contains a `build` stage. It is used to build our documentation pages which are based on
docusaurus.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/documentation/docusaurus.yml"

documentation:build:
  extends: .docusaurus:build
```

## Configuration

The template inherits all configuration options from [base](../base.md#configuration).

Additionally the following has to be configured

| Variable                    | Default value                                            | Description                                                                              |
|-----------------------------|----------------------------------------------------------|------------------------------------------------------------------------------------------|
| `DOCUSAURUS_TITLE`          | none, this is required                                   | The title to be shown on your docusaurus site                                            |
| `PROJECT_PATH_WITHOUT_ROOT` | none, this is required                                   | The path of your gitlab path without the root group. For example: `abfelbaum/ci` -> `ci` |
| `DOCUSAURUS_IMAGE_NAME`     | `registry.git.abfelbaum.dev/abfelbaum/images/docusaurus` | The container image name to be used                                                      |
| `DOCUSAURUS_IMAGE_TAG`      | `latest`                                                 | The container image tag to be used                                                       |
| `DOCUSAURUS_URL`            | `$CI_PAGES_URL`                                          | The url to be used for your docusaurus instance                                          |
| `DOCUSAURUS_PROJECT_URL`    | `$CI_PROJECT_URL`                                        | The url of the GitLab the documentation belongs to                                       |
| `DOCUSAURUS_LANGUAGE`       | `en`                                                     | The language your content will be written in                                             |
| `DOCUSAURUS_LOCATION`       | `docs`                                                   | The path of the docusaurus installation                                                  |
| `DOCUSAURUS_OUTPUT`         | `$CI_PROJECT_DIR/public`                                 | The path where the build output should be placed                                         |