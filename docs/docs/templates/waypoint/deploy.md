# Deploy

:::caution

The feature flag for [non-public artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifactspublic) has to be activated on
the GitLab instance.

:::

The deploy template contains a `deploy` stage. It is used to deploy waypoint applications.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file:
      - "templates/waypoint/build.yml"
      - "templates/waypoint/deploy.yml"

waypoint:build:
  extends: .waypoint:build

waypoint:deploy:
  extends: .waypoint:deploy
  dependencies:
    - waypoint:build
```

The stage just runs `waypoint deploy`. It requires the [build job](build.md) to have run before and needs its artifact
containing `data.db`.