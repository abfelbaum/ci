# Pack

The pack template contains a `build` stage. It is used to build NuGet packages.

:::info

To build normal .NET projects please refer to [my build CI](build.md).

:::

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/pack.yml"

dotnet:build:
  extends: .dotnet:pack
  variables:
    DOTNET_PROJECT_PATH: "path/to/your/projectfile.csproj"
```

## Configuration

The template inherits all configuration options from [build](build.md#configuration).