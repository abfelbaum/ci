# Base

The base template contains a `build` stage. It is used so .NET version configuration can be done centrally

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/base.yml"

dotnet:base:
  extends: .dotnet:base
```

:::note

The base template has no `script` so it will do anything without modification.

:::

## Configuration

The template inherits all configuration options from [base](base.md#configuration).

Additionally the following has to be configured

| Variable                   | Default value                                            | Description                                        |
|----------------------------|----------------------------------------------------------|----------------------------------------------------|
| `DOTNET_PROJECT_PATH`      | none                                                     | The project file to build                          |
| `DOTNET_TEST_PROJECT_PATH` | none                                                     | The test project to test                           |
| `DOTNET_BUILD_PATH`        | `build`                                                  | The path where to put the build output             |
| `DOTNET_PUBLISH_PATH`      | `publish`                                                | The path where to put the publish output           |
| `DOTNET_PACK_PATH`         | `pack`                                                   | The path where to put the generated `.nupkg` files |
| `DOTNET_TEST_PATH`         | `test`                                                   | The path where to put the test output              |
| `DOTNET_IMAGE_TAG`         | `lts`                                                    | The container image tag to be used                 |
| `DOTNET_IMAGE_NAME`        | `registry.git.abfelbaum.dev/abfelbaum/images/dotnet/sdk` | The container image name to be used                |
