# Test

The pack template contains a `test` stage. It is used to test .NET projects
using `[dotnet test](https://learn.microsoft.com/en-us/dotnet/core/tools/dotnet-test)`.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/test.yml"

dotnet:test:
  extends: .dotnet:test
  variables:
    DOTNET_TEST_PROJECT_PATH: "path/to/your/projectfile.csproj"
```

## Configuration

The template inherits all configuration options from [build](build.md#configuration).

This template will be ommitted when the `DOTNET_TEST_PATH` and `DOTNET_TEST_PROJECT_PATH` are not set.