# Build

The build template contains a `build` stage. It is used to build .NET projects.

:::info

To build NuGet packages please refer to [my pack CI](pack.md).

:::

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/build.yml"

dotnet:build:
  extends: .dotnet:build
  variables:
    DOTNET_PROJECT_PATH: "path/to/your/projectfile.csproj"
```

## Configuration

The template inherits all configuration options from [base](base.md#configuration).
