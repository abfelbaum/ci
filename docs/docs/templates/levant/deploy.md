# Deploy

The deploy template contains a `deploy` stage. It is used to deploy nomad applications.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file:
      - "templates/levant/deploy.yml"

levant:deploy:
  extends: .levant:deploy
```

The stage just runs `levant deploy`.

## Configuration

The template inherits all configuration options from [base](../base.md#configuration).

Levant also needs a file with some variables set.

```json title=levant.json
{
  "projectname": "Weather.Updater",
  "jobname": "weather-updater"
}
```

You also need to decide if you want to use a preconfigured template or use an own nomad or template file.

| Variable                        | Default value                                                                                          | Description                                    |
|---------------------------------|--------------------------------------------------------------------------------------------------------|------------------------------------------------|
| `LEVANT_ARTIFACT`               | `$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/$PREVIOUS_JOB_ID/artifacts.zip?private_token=$CI_API_KEY` | The url to the artifact of the build job.      |
| `LEVANT_DATACENTER`             | `app-testing`                                                                                          | The nomad datacenter.                          |
| `LEVANT_DEPLOYMENT_ENVIRONMENT` | `Staging`                                                                                              | The environment the deployment will happen in. |
| `LEVANT_JOB_TYPE`               | `service`                                                                                              | The type of the template to use.               |
| `LEVANT_JOB_FILE`               | `templates/$LEVANT_JOB_TYPE.tpl.nomad`                                                                 | The path to the template or job file.          |
| `LEVANT_NAMESPACE`              | `app`                                                                                                  | The nomad namespace.                           |
