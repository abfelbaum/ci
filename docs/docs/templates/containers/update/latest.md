# Latest

The latest template contains a `deploy` stage. It is used to trigger the stable latest version tag to rebuild
containers.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/containers/update/latest.yml"

containers:update:latest:
  extends: .containers:update:latest
```

## What it does

Every time this job is triggered it takes all git tags of the project and filters them for stable releases.

When the latest stable release is found a pipeline with the variables `SCHEDULE_CONTAINER_UPDATE_LATEST=true`
and `SCHEDULE_CONTAINER_UPDATE=true` will be triggered.

When no stable release is found the job exits gracefully.