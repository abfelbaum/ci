# Legacy

The legacy template contains a `deploy` stage. It is used to trigger the stable legacy version tags to rebuild
containers.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/containers/update/legacy.yml"

containers:update:legacy:
  extends: .containers:update:legacy
```

## Configuration

| Variable                       | Default value | Description                   |
|--------------------------------|---------------|-------------------------------|
| `CONTAINER_UPDATE_MIN_VERSION` | `0.0.0`       | The minimum version to update |

## What it does

Every time this job is triggered it takes all git tags of the project and filters them for stable releases that are
newer then `CONTAINER_UPDATE_MIN_VERSION`.

When the releases are found for each of the releases a pipeline with the
variables `SCHEDULE_CONTAINER_UPDATE_LATEST=false` and `SCHEDULE_CONTAINER_UPDATE=true` will be triggered.

When no stable release is found the job exits gracefully.