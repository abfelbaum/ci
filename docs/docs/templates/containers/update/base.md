# Base

The base template contains a `deploy` stage. It is used so container update configuration can be done centrally.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/containers/update/base.yml"

containers:update:base:
  extends: .containers:update:base
```

:::note

The base template has no `script` so it will do anything without modification.

:::