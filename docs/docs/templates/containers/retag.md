# Re-Tag

The re-tag template contains a `build` stage. It is used to pull a container from one registry and re-push it to
another.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title="gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/containers/retag.yml"

containers:retag:
  extends: .containers:retag
```

### Configure

Additional configuration

| Variable                   | Default value                                      | Description                                      |
|----------------------------|----------------------------------------------------|--------------------------------------------------|
| `CONTAINER_IMAGE_NAME`     | `$CI_REGISTRY_IMAGE`                               | The image without a tag name                     |
| `CONTAINER_OLD_TAG`        | `$CI_COMMIT_SHA`                                   | The tag of the old image                         |
| `CONTAINER_OLD_IMAGE_NAME` | `${CONTAINER_IMAGE_NAME}`                          | The image name of the old image                  |
| `CONTAINER_OLD_IMAGE`      | `${CONTAINER_OLD_IMAGE_NAME}:${CONTAINER_OLD_TAG}` | The image name and tag of the old image combined |
| `CONTAINER_NEW_TAG`        | `latest`                                           | The tag of the new image                         |
| `CONTAINER_NEW_IMAGE_NAME` | `${CONTAINER_IMAGE_NAME}`                          | The image name of the new image                  |
| `CONTAINER_NEW_IMAGE`      | `${CONTAINER_NEW_IMAGE_NAME}:${CONTAINER_NEW_TAG}` | The image name and tag of the new image combined |
                                              
