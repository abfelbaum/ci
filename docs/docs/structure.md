# Structure

The CI files in this project have one of two types:

## Templates

Template files contain the "business logic" of different CI stages. The `script`s, `artifact`s, essential `rules` (i.e.
checking for required environment variables) and more are defined here. They are intended to be used
by [General CI files](#general-ci-file) but can also be used in custom CI files for custom use cases.

## General CI file

General CI files are files that can be used in any (matching) project. Their functionality is based
on [template](#templates) files that are brought in order. General CI files also define the conditions when the
different stages are run. They can be viewed as a composition of multiple templates to create a ready to use GitLab CI.

## Examples

You can have a look at the `documentation.yml` CI file to get a sense of how this works.

A template for building the documentation can be found in `templates/documentation/docusaurus.yml`. Everything that is
needed to run the build is defined there in the `.docusaurus:build` template. Also, the template extends the `.base`
template which (or a inheritor of that) is recommended for all templates to provide a centralized configuration option.

Now you can have a look at the `documentation.yml`. There are various stages, but none of them contains any
functionality (`script`s and stuff). The build stage extends `.docusaurus:build` and defines *when* it is run, but not
*what* is run there.