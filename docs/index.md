---
sidebar_position: 1
---

# GitLab CI / CD

:::caution

The contents in this project are subject to unannounced (and undocumented) breaking changes.

If you want to use this CI configuration please use either permalinks or clone the project and review the changes before
any update.

:::

This project provides various GitLab CI templates for usage in my projects.

General CI files that are not intended for use in a specific setup are located in the `root` folder.

All CI files that do not use GitLab provided templates are based on templates in the `templates/` folder.
