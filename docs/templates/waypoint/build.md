# Build

:::caution

The feature flag for [non-public artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifactspublic) has to be activated on
the GitLab instance.

:::

The build template contains a `build` stage. It is used to build waypoint applications.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/waypoint/build.yml"

waypoint:build:
  extends: .waypoint:build
```

The stage just runs `waypoint init` followed by `waypoint build`. The generated `data.db` is uploaded to a non-public
artifact for [deploy job](deploy.md).