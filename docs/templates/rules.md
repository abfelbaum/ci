---
sidebar_position: 2
---

# Rules

The rules template provides rules that can be used in other pipelines.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/rules.yml"
```

Then you can add the rules to your jobs

```yaml title=".gitlab-ci.yml"
container_scanning:
  needs: 
    - waypoint:build:release:tmp
  rules:
    - !reference [ .is_release_stable, rules ]
    - !reference [ .is_release_preview, rules ]
    - !reference [ .is_container_update_legacy, rules ]
    - !reference [ .is_container_update_latest, rules ]
```

## Rules

### `.is_release_stable`

The rule checks if a tag name matches the semantic versioning pattern of stable releases (i.e. without `-beta`,
only `1.0.0`).

It only is triggered by newly pushed tags.

### `.is_release_preview`

The rule checks if a tag name matches the semantic versioning pattern of prereleases (i.e. with `-beta`, `1.0.0-beta`).

It only is triggered by newly pushed tags.

### `.is_container_update_legacy`

The rule checks if the pipeline is a container update and if `CI_CONTAINER_UPDATE_LATEST` is `false`.

### `.is_container_update_latest`

The rule checks if the pipeline is a container update and if `CI_CONTAINER_UPDATE_LATEST` is `true`.
