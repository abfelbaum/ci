---
sidebar_position: 1
---

# Base

The base template contains is the base of every other template. It is there to be able to do configuration changes for all images at once.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/base.yml"

base:
  extends: .base
```

:::note

The base template has no `script` so it will do anything without modification.

:::

## Configuration

:::info

Since all templates inherit from this template these configuration options are available for **every** other template.

:::

The template provides the following configuration options

| Variable          | Default value                                         | Description           |
|-------------------|-------------------------------------------------------|-----------------------|
| `BASE_IMAGE_TAG`  | `latest`                                              | The image tag to use  |
| `BASE_IMAGE_NAME` | `registry.git.abfelbaum.dev/abfelbaum/images/builder` | The image name to use |