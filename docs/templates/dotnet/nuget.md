# NuGet

The NuGet template contains two `deploy` stage. It is used to publish .NET packages to a registry.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title="gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/nuget.yml"

nuget:deploy:local:
  extends: .nuget:deploy:local
```

### Configure .NET project

It is recommended that you make use of the environment variables provided by GitLab to configure your package.

When you use [my release pipeline](../../release.md) you can safely use the following snippet

```xml title="YourProject.csproj"

<Project Sdk="Microsoft.NET.Sdk">
    <PropertyGroup>
        <IsPackable>true</IsPackable>
        <PackageVersion>$(CI_COMMIT_TAG)</PackageVersion>
        <PackageProjectUrl>$(CI_PROJECT_URL)</PackageProjectUrl>
        <RepositoryUrl>$(CI_REPOSITORY_URL)</RepositoryUrl>
        <RepositoryType>git</RepositoryType>
        <PackageReleaseNotes>$(CI_COMMIT_MESSAGE)</PackageReleaseNotes>
    </PropertyGroup>
</Project>
```

## Deploy to local NuGet registry

To deploy to the local NuGet registry that comes with GitLab use the `.nuget:deploy:local` template.

```yaml title="gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/nuget.yml"

nuget:deploy:local:
  extends: .nuget:deploy:local
```

### Configuration

The template inherits all configuration options from [base](base.md#configuration).

## Deploy to NuGet.org

To deploy to the NuGet.org use the `.nuget:deploy` template.

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/nuget.yml"

nuget:deploy:local:
  extends: .nuget:deploy
```

### Configuration

The template inherits all configuration options from [base](base.md#configuration).

Additionally the following has to be configured

| Variable           | Default value          | Description                                                                                                         |
|--------------------|------------------------|---------------------------------------------------------------------------------------------------------------------|
| `NUGET_TOKEN`      | none, this is required | The api key from [NuGet.org](https://learn.microsoft.com/en-us/nuget/nuget-org/publish-a-package#create-an-api-key) |
