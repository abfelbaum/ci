# Build

The build template contains a `build` stage. It is used to build .NET projects.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/dotnet/build.yml"

dotnet:build:
  extends: .dotnet:build
  variables:
    DOTNET_PROJECT_PATH: "path/to/your/projectfile.csproj"
```

The stage stores the `bin` and `obj` folders as artifacts for further use in the following stages. The nuget cache
should get stored in a cache:

```yaml title=".gitlab-ci.yml"
cache:
  key: "$CI_COMMIT_SHA"
  paths:
    - '**/bin/project.assets.json'
    - '**/bin/*.csproj.nuget.*'
    - '$DOTNET_RESTORE_PATH'
```

## Configuration

The template inherits all configuration options from [base](base.md#configuration).
