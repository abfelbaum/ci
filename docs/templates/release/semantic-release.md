# semantic-release

This template makes use of [semantic-release](https://github.com/semantic-release/semantic-release).

The job is created on pipelines that are not on `tags`, `pipelines` or `schedules`.

The reason for that is that this job needs to be triggered manually and should not be executed over existing releases (`tags`) or automatically by accident (`pipelines` and `schedules`).

## Getting started

### Configuration

The job expects configured variables for commiting, authenticating and signing commits.

#### Commiting

You *need* to configure the following environment variables for creating commits. I am not entirely sure which variables are used by `semantic-release` but I really don"t care since I"ve set up them once and it works.

| Variable              | Purpose                       |
|-----------------------|-------------------------------|
| `GIT_AUTHOR_EMAIL`    | The email of the git author   | 
| `GIT_AUTHOR_NAME`     | The name of the git author    |
| `GIT_COMMITTER_EMAIL` | The email of the git commiter |
| `GIT_COMMITTER_NAME`  | The name of the git commiter  |
| `GIT_EMAIL`           | The git email                 |
| `GIT_USERNAME`        | The git username              |

#### Authentication

##### git

Please refer to the [official documentation](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/ci-configuration.md#authentication).

I am just using the `GIT_CREDENTIALS` environment variable since it is the easiest and fastest way. It is mapped from `RELEASE_GIT_CREDENTIALS`.

##### GitLab

Please refer to the [official documentation](https://github.com/semantic-release/gitlab#gitlab-authentication).

#### GPG signing

If you have not configured commit signing please read [here](#disable-commit-signing) how to disable it. 

:::caution

Currently I don"t have the time to document this. Please either reverse engineer the template or [disable commit signing](#disable-commit-signing)

:::

## Customize

You can use this template for your own pipelines by extending the template job

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/release/semantic-release.yml"

semantic-release:deploy:
  extends: .semantic-release:deploy

  when: on_success # Do not wait for manual trigger
```

### Disable commit signing

To disable commit signing you just have to overwrite the `before_script`
```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "templates/release/semantic-release.yml"

semantic-release:deploy:
  extends: .semantic-release:deploy

  before_script:
    - echo "Do nothing"
```
