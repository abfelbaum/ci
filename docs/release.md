---
sidebar_position: 3
---

# Releasing versions

The `release.yml` makes use of `semantic-release` and GitLabs `release-cli` to automatically generate changelogs, tags and releases.

## Getting started

### Configuration

To make use of `semantic-release` you first need to create a configuration file in the root directory of your project

```js title="release.config.js"
const branch = process.env.CI_COMMIT_BRANCH

const config = {
    tagFormat: "${version}",
    branches: [
        "+([0-9])?(.{+([0-9]),x}).x",
        "main",
        {
            name: "dev",
            channel: "default",
            prerelease: "beta"
        }
    ],
    plugins: [
        ["@semantic-release/commit-analyzer", {
            preset: "conventionalcommits"
        }],
        ["@semantic-release/release-notes-generator", {
            preset: "conventionalcommits"
        }],
        ["@semantic-release/gitlab", {
            labels: "Type::fix,From::Release Bot,Priority::High"
        }]
    ]
}

if (config.branches.some(it => it === branch || (it.name === branch && !it.prerelease))) {

    config.plugins.push([ "@semantic-release/changelog", {
        changelogTitle: "# Changelog\n\nAll notable changes to this project will be documented in this file. See\n[Conventional Commits](https://conventionalcommits.org) for commit guidelines."
    }]);

    config.plugins.push([ "@semantic-release/git", {
        message: "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
    }]);
}

if(branch !== "main" && branch !== "dev") {
    config.branches.push({
        name: branch,
        channel: "default",
        prerelease: "alpha"
    })
}

module.exports = config
```

This will configure semantic release to:

* Use [ConventionalCommits](https://www.conventionalcommits.org/en/v1.0.0/)
* Create `stable` releases from `main` branch
* Create `beta` releases from `dev` branch
* Create `alpha` releases from any other branch
* Create specific version releases from `major.x` or `major.minor.x` branches
* Generate changelog only on stable branches

### GitLab CI

When you configured `semantic-release` you can add the `release.yml` to your GitLab CI

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "release.yml"
```

It is using the `deploy` stage.

:::info

If you have **not** configured GPG singing please refer to [this section](templates/release/semantic-release.md#disable-commit-signing).

:::

:::info

If you prefer to not use the GitLab plugin, feel free to comment it out or remove it from the configuration file.

:::

Now, for every commit you have a manual stage named `semantic-release:deploy`. The following happens on trigger

1. `semantic-release` analyzes the commit messages and determines the next version
2. (stable branches only) `semantic-release` generates `CHANGELOG.md`
3. `semantic-release` creates a new tag
4. `semantic-release` creates a new release with the tag name as title and the commit message as description
5. `semantic-release` comments to every issue that was resolved in that release that it has been resolved
