---
sidebar_position: 4
---

# Security scanning

`security.yml` includes the most important GitLab security (and compliance) scanners.

## Getting started

To include the security scans in your project include the `security.yml` in your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "security.yml"
```

## Configuration

Since these scanners are built in the configuration is documented at GitLab.

| Job                   | Documentation                                                             |
|-----------------------|---------------------------------------------------------------------------|
| `dependency_scanning` | https://docs.gitlab.com/ee/user/application_security/dependency_scanning/ |
| `container_scanning`  | https://docs.gitlab.com/ee/user/application_security/container_scanning/  |
| `*-sast`              | https://docs.gitlab.com/ee/user/application_security/sast/                |
| `secret_detection`    | https://docs.gitlab.com/ee/user/application_security/secret_detection/    |
| `license_scanning`    | https://docs.gitlab.com/ee/user/compliance/license_compliance/            |

## Disable job

To disable a specific job you can add a rule that never equals true to it

```yaml title=".gitlab-ci.yml"
container_scanning:
  rules:
    - when: never
```