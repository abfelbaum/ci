---
description: "The most important GitLab security (and compliance) scanners for containers."
---

# Security scanning

`containers/security.yml` includes the most important GitLab security (and compliance) scanners for containers.

## Getting started

To include the security scans in your project include the `containers/security.yml` in your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "containers/security.yml"
```

## Configuration

Since these scanners are built in the configuration is documented at GitLab

| Job                  | Documentation                                                            |
|----------------------|--------------------------------------------------------------------------|
| `container_scanning` | https://docs.gitlab.com/ee/user/application_security/container_scanning/ |
| `secret_detection`   | https://docs.gitlab.com/ee/user/application_security/secret_detection/   |

## Disable job

To disable a specific job you can add a rule that never equals true to it

```yaml title=".gitlab-ci.yml"
container_scanning:
  rules:
    - when: never
```