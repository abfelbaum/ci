---
description: "Re-tag and re-upload containers"
---

# Re-Tag

Sometimes containers do not need to be built because there is already an existing container. This container can be
re-tagged and uploaded to your own registry.

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "containers/retag.yml"
```

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                                  | Type     | 
|-------------------------------------------------------|----------|
| [containers re-tag](../templates/containers/retag.md) | Template |
