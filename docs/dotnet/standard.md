---
sidebar_position: 1
description: "Building and deploying a .NET project with waypoint."
---

# Standard .NET CI

This ci file handles everything from building to deploying a .NET project with waypoint.

:::info

To build NuGet packages please refer to [my NuGet CI](nuget.md).

:::

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "dotnet/standard.yml"

variables:
  DOTNET_PROJECT_PATH: "path/to/your/projectfile.csproj"
```

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                                  | Type     | 
|-------------------------------------------------------|----------|
| [Release](../release.md)                              | CI       | 
| [Security](../security.md)                            | CI       | 
| [Containers update](../containers/update.md)          | CI       | 
| [Containers re-tag](../templates/containers/retag.md) | Template | 
| [.NET build](../templates/dotnet/build.md)            | Template | 
| [waypoint build](../templates/waypoint/build.md)      | Template | 
| [waypoint deploy](../templates/waypoint/deploy.md)    | Template |

Additional configuration

| Variable   | Default value                           | Description                                                                                                                                        |
|------------|-----------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| `CS_IMAGE` | `${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}` | The container image that should be scanned by the `container_scanning` job. Has to always refer to the image with the current commit hash as a tag |

## What it does

The template triggers at every commit a .NET build. A container build can be triggered manually.

For every pipeline a manual job for releasing is added.

On a release a docker container is built and pushed to the container registry. For stable releases the container is then
deployed to production.