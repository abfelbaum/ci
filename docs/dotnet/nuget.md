---
sidebar_position: 2
description: "Building and deploying a NuGet package."
---

# NuGet Packages

This `dotnet/nuget.yml` ci file handles everything from building to deploying a NuGet package.

:::info

To build normal .NET projects please refer to [my dotnet CI](standard.md).

:::

## Getting started

Add the following to your `.gitlab-ci.yml`

```yaml title=".gitlab-ci.yml"
include:
  - project: "abfelbaum/ci"
    file: "dotnet/nuget.yml"

variables:
  DOTNET_PROJECT_PATH: "path/to/your/projectfile.csproj"
```

### Configuration

The ci configuration depends on various templates that all have their own configuration. Please have a short look at
them in case they need any configuration for your use case.

| Name                                       | Type     | Notes                                                                      |
|--------------------------------------------|----------|----------------------------------------------------------------------------|
| [Release](../release.md)                   | CI       | -                                                                          |
| [Security](../security.md)                 | CI       | `container_scanning` job is deactivated since there are no containers here |
| [.NET nuget](../templates/dotnet/nuget.md) | Template | -                                                                          |
| [.NET pack](../templates/dotnet/pack.md)   | Template | -                                                                          |

## What it does

The template triggers at every commit a .NET build.

For every pipeline a manual job for releasing is added.

On a release a NuGet package is built and pushed to the local NuGet registry. The NuGet package is also pushed
to [NuGet.org](https://nuget.org)